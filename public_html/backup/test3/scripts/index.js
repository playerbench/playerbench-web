$(document).ready(function() {
  $('#scroll').click(function() {
    event.preventDefault();
    $('html,body').animate({scrollTop: ($("#summary").offset().top)}, 1000);
  });

  $('#scrollSmall').click(function(event) {
    event.preventDefault();

    $('html,body').animate({scrollTop: ($("#summary").offset().top)}, 1000);
  });

  $('#backToTop').click(function(event) {
    event.preventDefault();

    $('html,body').animate({scrollTop: ($("#navBar").offset().top)}, 800);
  });
});
