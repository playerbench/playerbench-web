$(document).ready(function() {
  $('#loginBtn').click(function() {
    var loginReg = new RegExp(/^[-a-z0-9~!$%^&*_=+}{\'?]+(\.[-a-z0-9~!$%^&*_=+}{\'?]+)*@([a-z0-9_][-a-z0-9_]*(\.[-a-z0-9_]+)*\.(aero|arpa|biz|com|coop|edu|gov|info|int|mil|museum|name|net|org|pro|travel|mobi|[a-z][a-z])|([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}))(:[0-9]{1,5})?$/i);
    var email = $('#login-username').val();
    var password = $('#login-password').val();

    if (loginReg.test(email)){
      $('#loaderGif').show();
      $('#invalidMsg').hide();

      aja()
        .method('get')
        .url('/api/login')
        .queryString({
          'email':email,
          'password':password
        })
        .on('200', function(response) {
          console.log(response);
          window.location.href = "/test2/index.html";
        })
        .go()

    } else {
      $("#invalidMsg").show();
      $('#loaderGif').hide();
    }
  });
});
