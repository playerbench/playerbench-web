$(document).ready(function() {

  if (Cookies.get('type') == null) {
    $('#loginTab').show();
    $('#gmTab').hide();
    $('#evalTab').hide();
    $('#cvTab').hide();
    $('#logout').hide();
  } else if (Cookies.get('type') == 2 || Cookies.get('type') == 3 || Cookies.get('type') == 5) {
    $('#loginTab').hide();
    $('#cvTab').hide();
    $('#gmTab').hide();
    $('#evalTab').show();
    $('#logout').show();
  } else if (Cookies.get('type') == 4) {
    $('#loginTab').hide();
    $('#cvTab').hide();
    $('#evalTab').hide();
    $('#gmTab').show();
    $('#logout').show();
  } else if (Cookies.get('type') == 1) {
    $('#loginTab').hide();
    $('#gmTab').hide();
    $('#evalTab').hide();
    $('#cvTab').show();
    $('#logout').show();
  }

  $('#scroll').click(function() {
    event.preventDefault();
    $('html,body').animate({scrollTop: ($("#summary").offset().top)}, 1000);
  });

  $('#scrollSmall').click(function(event) {
    event.preventDefault();

    $('html,body').animate({scrollTop: ($("#summary").offset().top)}, 1000);
  });

  $('#backToTop').click(function(event) {
    event.preventDefault();

    $('html,body').animate({scrollTop: ($("#navBar").offset().top)}, 800);
  });
});
