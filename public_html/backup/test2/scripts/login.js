$(document).ready(function() {

  $('#loginBtn').click(function() {
    var loginReg = new RegExp(/^[-a-z0-9~!$%^&*_=+}{\'?]+(\.[-a-z0-9~!$%^&*_=+}{\'?]+)*@([a-z0-9_][-a-z0-9_]*(\.[-a-z0-9_]+)*\.(aero|arpa|biz|com|coop|edu|gov|info|int|mil|museum|name|net|org|pro|travel|mobi|[a-z][a-z])|([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}))(:[0-9]{1,5})?$/i);
    var email = $('#login-username').val();
    var password = $('#login-password').val();

    if (loginReg.test(email)){
      $('#loaderGif').show();
      $('#invalidMsg').hide();

      aja()
        .method('get')
        .url('/api/login')
        .queryString({
          'email':email,
          'password':password
        })
        .on('200', function(response) {
          console.log(response);
          Cookies.set('email', email);
          if(response.parent == 1) {
              Cookies.set('type', 1);
              window.location.href = "/test2/cv.html";
          } else if (response.coach == 1) {
              Cookies.set('type', 2);
              window.location.href = "/test2/home.html";
          } else if (response.techDir == 1) {
              Cookies.set('type', 3);
              window.location.href = "/test2/home.html";
          } else if (response.genManag == 1) {
              Cookies.set('type', 4);
              window.location.href = "/test2/gm.html";
          } else if (response.ageGroupCoord == 1) {
              Cookies.set('type', 5);
              window.location.href = "/test2/gm.html";
          } else {
              console.log("Not a User Type");
          }
        })
        .on('404', function(response) {
          $("#invalidMsg").show();
          $('#loaderGif').hide();
        })
        .go()
    } else {
      $("#invalidMsg").show();
      $('#loaderGif').hide();
    }
  });
});
