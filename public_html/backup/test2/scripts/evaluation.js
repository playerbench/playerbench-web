window.onload = function() {

    function check() {
      if (Cookies.get('email') == null || Cookies.get('type') == null) {
        window.location.href = "index.html";
      }
    }

    check();

    //var accountEmail = "testCoach@test.com";
    var accountEmail = Cookies.get('email');
    var accountType;
    var club = Cookies.get('club');
    var attributes = [];
    var ageGroupSelected;
    var teamSelected;
    var playerSelected = 0;
    var accordion = [];
    var chart;
    var ageGroups = [];
    var players = [];
    var agTeams = [];
    var evaluationID =[];
    var abilityCheck = $("#graphAbility");
    var attitudeCheck = $("#graphAttitude");
    var attendanceCheck = $("#graphAttendance");
    var evaluationList = [];

    if (Cookies.get('type') == 1) {
      window.location.replace = "cv.html";
    } else if (Cookies.get('type') == 2) {
      $("#setAttributes").hide();
    } else {
      $("#setAttributes").show();
    }

    if (Cookies.get('type') == 2) {
        accountType = "Coach";
    } else if (Cookies.get('type') == 3) {
        accountType = "TD";
    } else if (Cookies.get('type') == 5) {
        accountType = "AgeC";
    }


    $("#evalSearch").click(function() {
        var date1 = new Date(document.getElementById("inputDateFrom").value);
        var date2 = new Date (document.getElementById("inputDateTo").value);
        var reviewer = $('input[name=searchRadio]:checked', '#searchForm').val();
        var dateFrom = document.getElementById("inputDateFrom").value;
        var dateTo = document.getElementById("inputDateTo").value;

        if(dateTo < dateFrom) {
            alert("Date from is a later date than date to. Enter them so date from comes before date to.");
            return;
        }
        if(date1 == "Invaild Date"|| date2 == "Invalid Date" || reviewer == null || dateFrom == null || dateTo == null) {
            alert("Not all Search fields were entered.");
            return;
        }

        aja()
        .method("get")
        .url('/api/getEvaluationRange')
        .queryString({
            'playerID': playerSelected,
            'dateFrom': dateFrom,
            'dateTo': dateTo,
            'evaluatorType': reviewer
        })
        .on('200', function(response){
            evaluationList = response;
            populateEval(response);
            populateGraph(response);
        })
        .on('404', function(response){
            console.log("Evaluations not found");
        })
        .go();


        if(playerSelected != 0) {
            clearEvalPage();
            createGraph();
        }
    });
    //attendanceRange.on("input change", updateAttendanceInput(attendanceRange))

    createGraph();

    //adds new points to graph
    function addPoints(points, date) {
        //attitude
        if(document.getElementById("graphAttitude").checked) {
            if(points[0] == 1) {
                chart.options.data[0].dataPoints.push({ x: date, y: 1});
            } else if (points[0] == 2) {
                chart.options.data[0].dataPoints.push({ x: date, y: 2});
            } else if (points[0] == 3) {
                chart.options.data[0].dataPoints.push({ x: date, y: 3});
            } else if (points[0] == 4) {
                chart.options.data[0].dataPoints.push({ x: date, y: 4});
            } else if (points[0] == 5) {
                chart.options.data[0].dataPoints.push({ x: date, y: 5});
            }

        }

        if(document.getElementById("graphAttendance").checked) {

            //attendance
            if(points[1] == 1) {
                chart.options.data[1].dataPoints.push({ x: date, y: 1});
            } else if (points[1] == 2) {
                chart.options.data[1].dataPoints.push({ x: date, y: 2});
            } else if (points[1] == 3) {
                chart.options.data[1].dataPoints.push({ x: date, y: 3});
            } else if (points[1] == 4) {
                chart.options.data[1].dataPoints.push({ x: date, y: 4});
            } else if (points[1] == 5) {
                chart.options.data[1].dataPoints.push({ x: date, y: 5});
            }

        }

        if(document.getElementById("graphAbility").checked) {
            //ability
            if(points[2] == 1) {
                chart.options.data[2].dataPoints.push({ x: date, y: 1});
            } else if (points[2] == 2) {
                chart.options.data[2].dataPoints.push({ x: date, y: 2});
            } else if (points[2] == 3) {
                chart.options.data[2].dataPoints.push({ x: date, y: 3});
            } else if (points[2] == 4) {
                chart.options.data[2].dataPoints.push({ x: date, y: 4});
            } else if (points[2] == 5) {
                chart.options.data[2].dataPoints.push({ x: date, y: 5});
            }
        }
    }
    //sets up data for graph
    function populateGraph(evaluations) {
        var splitEventDate = [];
        var date;
        var name;
        //var date = new Date()
        var points = [];
        for(var ep = 0; ep < evaluations.length; ep++) {
            splitEventDate = evaluations[ep].eventDate.split("-");

            date = new Date(parseInt(splitEventDate[0]), (parseInt(splitEventDate[1]) - 1), parseInt(splitEventDate[2]));

            points[0] = evaluations[ep].attitude;
            points[1] = evaluations[ep].attendance;
            points[2] = evaluations[ep].ability;
            addPoints(points, date);
        }
        for(var j = 0; j < players.length; j++){
            if(players[j][0] == playerSelected) {
                name = players[j][1] + " " + players[j][2];
            }
        }

        chart.options.title.text = name + " Progression";

        chart.render();
    }

    function updateGraph(){
        if(playerSelected != 0) {
            $("#chartContainer").empty();
            createGraph();

            populateGraph(evaluationList);
        }
    }

    abilityCheck.click(updateGraph);
    attitudeCheck.click(updateGraph);
    attendanceCheck.click(updateGraph);

    function updateEval() {
        if(playerSelected != 0) {
            clearEvalPage();
            createGraph();
        }

        aja()
        .method("get")
        .url('/api/getEvaluations')
        .queryString({
            'playerID': playerSelected,
            'email': accountEmail
        })
        .on('200', function(response){
            evaluationList = response;
            populateEval(response);
            populateGraph(response);
        })
        .on('404', function(response){
            console.log("Evaluations not found");
        })
        .go();
    }

    $("#CRAdd").click(function() {
        aja()
        .method("post")
        .url('/api/updateEvaluation')
        .queryString({
            eventID: evaluationID[0],
            playerID: evaluationID[1],
            attendance: $("input[name=optradio]:checked").val(),
            attendanceComment: $("#attendComment").val(),
            attitude: $("#attitudeRange").val(),
            attitudeComment: $("#attitudeComment").val(),
            ability: $("#abilityRange").val()
        })
        .on('200',  function(response){
            createTDAccordion(response);
        })
        .on('404', function(response){
            console.log("Age Groups not found");
        })
        .go();

        updateEval();
        $("#editEvalForm").trigger("reset");
        $('#attitudeLabel').text(" " + $('#attitudeRange').val());
        $('#abilityLabel').text(" " + $('#abilityRange').val());
    });

    //clears Evaluation page
    function clearEvalPage() {
        $("#practiceEvalBody").empty();
        $("#gamesEvalBody").empty();
        $("#tournamentsEvalBody").empty();
        $("#chartContainer").empty();
    }

    //Switches Evaluation page
    function switchEvalPage() {
        if(playerSelected != 0) {
            clearEvalPage();
            createGraph();
        }
        playerSelected = this.id;

        aja()
        .method("get")
        .url('/api/getEvaluations')
        .queryString({
            'playerID': playerSelected,
            'email': accountEmail
        })
        .on('200', function(response){
            evaluationList = response;
            populateEval(response);
            populateGraph(response);
        })
        .on('404', function(response){
            console.log("Evaluations not found");
        })
        .go();
    }

    function setEvaluationID() {
        var buttonID = this.id;
        evaluationID = buttonID.split(" ");
    }

    function getEvaluationID() {
        //evaluationID[0] = eventID, evaluationID[1] = playerID
        return evaluationID;
    }

    function setAttributeSelection(ageGroups) {
        var SetAttriAgeGroup = document.getElementById("AttriAgeGroup");

        for(var ag = 0; ag < ageGroups.length; ag++) {
            var op = document.createElement("option");
            var opText = document.createTextNode(ageGroups[ag].ageGroup);

            op.setAttribute("value", ageGroups[ag].ageGroup);
            op.appendChild(opText);

            SetAttriAgeGroup.appendChild(op);
        }
    }

    function setAttributes(attr) {
        attributes = attr;
        console.log(attributes);
        addAttributesToGraph();
    }

    function setAgeGroups(ag) {
        for(var m = 0; m < ag.length; m++) {
            ageGroups[m] = [];
            ageGroups[m][0] = ag[m].ageGroup;
            ageGroups[m][1] = ag[m].ageGroupID;
        }
        addAgeGroupsToGraph();

        aja()
        .method("get")
        .url('/api/getAttributes')
        .queryString({
            clubID: club
        })
        .on('200', function(response){
            setAttributes(response);
        })
        .go();
    }

    //if account is a TD
    if(accountType == "TD") {
        aja()
        .method("get")
        .url('/api/getTDTeams')
        .queryString({
            email: accountEmail
        })
        .on('200',  function(response){
            $("#setAttributes").show();
            setAgeGroups(response);
            createTDAccordion(response);
            setAttributeSelection(response);
        })
        .on('404', function(response){
            console.log("Age Groups not found");
        })
        .go();
    }

    //If account is a Coach or Age group coordinator
    if(accountType == "Coach") {
        aja()
        .method("get")
        .url('/api/getCoachTeams')
        .queryString({
            email:accountEmail
        })
        .on('200', function(response){
            createAccordion(response);
        })
        .on('404', function(response){
            console.log("Teams not found");
        })
        .go();
    }

    //If account is a Coach or Age group coordinator
    if(accountType == "AgeC") {
        aja()
        .method("get")
        .url('/api/getCoordinatorTeams')
        .queryString({
            email:accountEmail
        })
        .on('200', function(response){
            createAccordion(response);
        })
        .on('404', function(response){
            console.log("Teams not found");
        })
        .go();
    }

    function createTDAccordion(ageGroups) {
        for(var ag = 0; ag < ageGroups.length; ag++) {
            for(var k = 0;  ageGroups[ag][k] != null; k++){
                for(var b = 0; ageGroups[ag][k][b] != null; b++){
                    players[b] = [];
                    players[b][0] = ageGroups[ag][k][b].playerID;
                    players[b][1] = ageGroups[ag][k][b].firstName;
                    players[b][2] = ageGroups[ag][k][b].lastName;
                }
            }
        }


        //panel-group
        var panelGroup = document.getElementById("accordion");

        for(var a = 0; a < ageGroups.length; a++) {
            var agePanelGroup = document.createElement("div");
            agePanelGroup.setAttribute("class", "panel panel-danger");
            panelGroup.appendChild(agePanelGroup);

            var agePanel  = document.createElement("div");

            agePanel.setAttribute("class", "panel-collapse");
            //agePanel.setAttribute("class", "panel-group");

            agePanelGroup.appendChild(agePanel);

            var agePanelHeading = document.createElement("div");
            agePanelHeading.setAttribute("class", "panel-heading");

            agePanel.appendChild(agePanelHeading);

            var agePanelHeader = document.createElement("h2");
            agePanelHeader.setAttribute("class", "panel-title");

            agePanelHeading.appendChild(agePanelHeader);

            var ageGroupButton = document.createElement("a");
            ageGroupButton.setAttribute("data-toggle", "collapse");
            ageGroupButton.setAttribute("data-parent", "#accordion");
            ageGroupButton.setAttribute("href", "#collapseU" + a);

            var ageGroupText = document.createTextNode(ageGroups[a].ageGroup);

            ageGroupButton.appendChild(ageGroupText);

            agePanelHeader.appendChild(ageGroupButton);

            agePanelHeader.style.textAlign = "center";

            var ageGlyphicon = document.createElement("span");
            ageGlyphicon.setAttribute("class", "glyphicon glyphicon-chevron-right");
            ageGlyphicon.style.cssFloat = "right";

            ageGroupButton.appendChild(ageGlyphicon);

            //Team panel
            var teamPanelGroup = document.createElement("div");

            teamPanelGroup.setAttribute("class", "panel-collapse collapse");
            teamPanelGroup.setAttribute("id", "collapseU" + a);

            agePanel.appendChild(teamPanelGroup);

            for( var t = 0; ageGroups[a][t] != null; t++) {

                var teamPanel = document.createElement("div");
                teamPanel.setAttribute("id", "collapseU" + a);

                teamPanel.setAttribute("class", "panel panel-default");

                teamPanelGroup.appendChild(teamPanel);

                var teamPanelHeading = document.createElement("div");
                teamPanelHeading.setAttribute("class", "panel-heading");

                teamPanel.appendChild(teamPanelHeading);

                var teamPanelHeader = document.createElement("h2");
                teamPanelHeader.setAttribute("class", "panel-title");

                teamPanelHeading.appendChild(teamPanelHeader);

                var teamButton = document.createElement("a");
                teamButton.setAttribute("data-toggle", "collapse");

                teamButton.setAttribute("data-parent", "#collapseU" + a);

                teamButton.setAttribute("href", "#collapse" + a + t);

                var teamText = document.createTextNode(ageGroups[a][t].teamName);

                teamButton.appendChild(teamText);

                teamPanelHeader.appendChild(teamButton);

                teamPanelHeader.style.textAlign = "center";

                var teamGlyphicon = document.createElement("span");
                teamGlyphicon.setAttribute("class", "glyphicon glyphicon-chevron-down");
                teamGlyphicon.style.cssFloat = "right";

                teamButton.appendChild(teamGlyphicon);

                var playerCollapseGroup = document.createElement("div");
                playerCollapseGroup.setAttribute("id", "collapse" + a + t);
                playerCollapseGroup.setAttribute("class", "panel-collapse collapse");

                teamPanelGroup.appendChild(playerCollapseGroup);

                var playerPanel = document.createElement("div");
                playerPanel.setAttribute("class", "panel-body");

                playerCollapseGroup.appendChild(playerPanel);

                var playerTable = document.createElement("table");
                playerTable.setAttribute("class", "table table-striped table-condensed");

                playerPanel.appendChild(playerTable);

                playerPanel.style.textAlign = "center";

                var playerTableBody = document.createElement("tbody");

                for(var p = 0; ageGroups[a][t][p] != null; p++) {

                    var playerTr = document.createElement("tr");
                    var playerTd = document.createElement("td");
                    var playerButton = document.createElement("a");

                    var playerText = document.createTextNode(ageGroups[a][t][p].firstName + " "
                    + ageGroups[a][t][p].lastName + " " + ageGroups[a][t][p].number);

                    playerButton.setAttribute("id", ageGroups[a][t][p].playerID);
                    playerButton.onclick = switchEvalPage;

                    playerButton.appendChild(playerText);

                    playerTd.appendChild(playerButton);

                    playerTr.appendChild(playerTd);

                    playerTableBody.appendChild(playerTr);

                    playerTable.appendChild(playerTableBody);

                }

            }

        }

    }

    function createAccordion(teams) {
        for(var k = 0; k < teams.length; k++){
            for(var b = 0; teams[k][b] != null; b++){
                players[b] = [];
                players[b][0] = teams[k][b].playerID;
                players[b][1] = teams[k][b].firstName;
                players[b][2] = teams[k][b].lastName;
            }
        }
        //panel-group
        var panelGroup = document.getElementById("accordion");
        var ageGroups = 1;


        for(var a = 0; a < ageGroups; a++) {
            //creates age panel if account is TD

            //Team panel
            var teamPanelGroup = document.createElement("div");
            teamPanelGroup.setAttribute("id", "collapseU" + a);

            //Checking account type
            teamPanelGroup.setAttribute("class","panel panel-danger");
            panelGroup.appendChild(teamPanelGroup);

            for( var t = 0; t < teams.length; t++) {

                var teamPanel = document.createElement("div");
                teamPanel.setAttribute("class", "panel panel-default");

                teamPanelGroup.appendChild(teamPanel);

                var teamPanelHeading = document.createElement("div");
                teamPanelHeading.setAttribute("class", "panel-heading");

                teamPanel.appendChild(teamPanelHeading);

                var teamPanelHeader = document.createElement("h2");
                teamPanelHeader.setAttribute("class", "panel-title");

                teamPanelHeading.appendChild(teamPanelHeader);

                var teamButton = document.createElement("a");
                teamButton.setAttribute("data-toggle", "collapse");

                //Sets to accordion if not a TD account
                teamButton.setAttribute("data-parent", "#accordion");
                teamButton.setAttribute("href", "#collapse" + a + t);

                var teamText = document.createTextNode(teams[t].teamName);

                teamButton.appendChild(teamText);

                teamPanelHeader.appendChild(teamButton);

                teamPanelHeader.style.textAlign = "center";

                var teamGlyphicon = document.createElement("span");
                teamGlyphicon.setAttribute("class", "glyphicon glyphicon-chevron-down");
                teamGlyphicon.style.cssFloat = "right";

                teamButton.appendChild(teamGlyphicon);

                var playerCollapseGroup = document.createElement("div");
                playerCollapseGroup.setAttribute("id", "collapse" + a + t);
                playerCollapseGroup.setAttribute("class", "panel-collapse collapse");

                teamPanelGroup.appendChild(playerCollapseGroup);

                var playerPanel = document.createElement("div");
                playerPanel.setAttribute("class", "panel-body");

                playerCollapseGroup.appendChild(playerPanel);

                var playerTable = document.createElement("table");
                playerTable.setAttribute("class", "table table-striped table-condensed");

                playerPanel.appendChild(playerTable);

                playerPanel.style.textAlign = "center";

                var playerTableBody = document.createElement("tbody");

                for(var p = 0; teams[t][p] != null; p++) {

                    var playerTr = document.createElement("tr");
                    var playerTd = document.createElement("td");
                    var playerButton = document.createElement("a");

                    var playerText = document.createTextNode(teams[t][p].firstName + " " + teams[t][p].lastName + " " + teams[t][p].number);
                    playerButton.setAttribute("id", teams[t][p].playerID);
                    playerButton.onclick = switchEvalPage;

                    playerButton.appendChild(playerText);

                    playerTd.appendChild(playerButton);

                    playerTr.appendChild(playerTd);

                    playerTableBody.appendChild(playerTr);

                    playerTable.appendChild(playerTableBody);

                }

            }

        }

    }

    function populateCommentModal() {
        $("#attributeCommentDiv").empty();
        $("#attributeCommentDiv").text(this.id);
    }

    function populateEval(evaluations) {


        for(var e = 0; e < evaluations.length; e++) {


            if(evaluations[e].eventType.localeCompare("Practice") == 0) {
                evalBody = document.getElementById("practiceEvalBody");

            } else if (evaluations[e].eventType.localeCompare("Game") == 0) {
                evalBody = document.getElementById("gamesEvalBody");

            } else {
                evalBody = document.getElementById("tournamentsEvalBody");

            }
            var infoRow = document.createElement("tr");
            var dateTD = document.createElement("td");
            var dateText = document.createTextNode("Date: " + evaluations[e].eventDate);
            var evalTypeTD = document.createElement("td");
            var evalTypeText = document.createTextNode(" Reviewer: " + evaluations[e].evaluatorType);
            var editTD =  document.createElement("td");
            var editButton = document.createElement("button");
            var buttonIcon = document.createElement("i");
            var buttonText = document.createTextNode("Edit ");
            var extraAttrCount = 0;
            dateTD.appendChild(dateText);
            evalTypeTD.appendChild(evalTypeText);
            infoRow.appendChild(dateTD);


            editButton.setAttribute("role", "button");
            editButton.setAttribute("class", "btn btn-success");
            editButton.setAttribute("role", "button");
            editButton.setAttribute("data-toggle", "modal");
            editButton.setAttribute("data-target", "#editEvaluation");
            editButton.setAttribute("id", evaluations[e].eventID + " " + evaluations[e].playerID);
            editButton.onclick = setEvaluationID;

            buttonIcon.setAttribute("class", "glyphicon glyphicon-pencil editButton");


            editButton.appendChild(buttonText);
            editButton.appendChild(buttonIcon);

            if(accountType == evaluations[e].evaluatorType) {
                editTD.appendChild(editButton);
            }


            //evalBody.appendChild(infoRow);
            for(var z = 0; z < 3; z++) {
                //var attributeRow = document.createElement("tr");
                var attributeTD = document.createElement("td");
                var commentButton = document.createElement("button");
                var commentButtonText = document.createTextNode("...");

                var text;
                var attributeText;
                var commentText = "";

                if(z == 0){
                    var attendance;
                    if(evaluations[e].attendance == 1) {
                        attendance = "Absent ";
                    } else if(evaluations[e].attendance == 3) {
                        attendance = "Late ";
                    } else if(evaluations[e].attendance == 5) {
                        attendance = "Present ";
                    }
                    text = "Attendance: " + attendance ;
                    commentText = evaluations[e].attendanceComment;
                } else if(z == 1) {
                    text = "Attitude: " + evaluations[e].attitude + " ";
                    commentText = evaluations[e].attitudeComment;
                } else {
                    text = "Ability: " + evaluations[e].ability;
                }
                attributeText = document.createTextNode(text);

                attributeTD.appendChild(attributeText);

                if(commentText != "") {
                    commentButton.setAttribute("id", commentText);
                    commentButton.setAttribute("class", "btn btn-default btn-xs");
                    commentButton.setAttribute("data-toggle", "modal");
                    commentButton.setAttribute("data-target", "#attributeComment");
                    commentButton.onclick = populateCommentModal;
                    commentButton.appendChild(commentButtonText);
                    attributeTD.appendChild(commentButton);

                }


                infoRow.appendChild(attributeTD);
                infoRow.appendChild(editTD);
            }


            infoRow.appendChild(editTD);
            evalBody.appendChild(infoRow);
            var newRow;
            for(var f = 13; evaluations[e][f] != null; f++) {
                if(evaluations[e][f] != null) {
                    var title;
                    if(extraAttrCount == 0 || extraAttrCount == 3) {

                        if(extraAttrCount == 3) {
                            evalBody.appendChild(newRow);
                        }

                        var emptyfirstTD = document.createElement("td");

                        extraAttrCount = 0;
                        newRow = document.createElement("tr");
                        newRow.appendChild(emptyfirstTD);
                    }
                    extraAttrCount++;
                    var newAttriEval = document.createElement("td");
                    var newAttriEvalText
                    switch (f) {
                        case 13:
                        title = "Game Sense: ";
                        break;
                        case 14:
                        title = "Energy/Work Rate: ";
                        break;
                        case 15:
                        title = "Technical Ability: ";
                        break;
                        case 16:
                        title = "Lost Possession Resp: ";
                        break;
                        case 17:
                        title = "Takes Initiative: ";
                        break;
                        case 18:
                        title = "First Touch: ";
                        break;
                        case 19:
                        title = "Passing Ability: ";
                        break;
                        case 20:
                        title = "Shooting Accuracy: ";
                        break;
                        case 21:
                        title = "1-on-1 Defending: ";
                        break;
                        case 22:
                        title = "Dribbling Ability: ";
                    }
                    newAttriEvalText = document.createTextNode(title + evaluations[e][f]);
                    newAttriEval.appendChild(newAttriEvalText);
                    newRow.appendChild(newAttriEval);
                }
            }

            if(extraAttrCount != 0) {
                evalBody.appendChild(newRow);
            }
        }
    }

    function createGraph() {
        var titleName = "";

        chart = new CanvasJS.Chart("chartContainer",
        {
            zoomEnabled: false,
            animationEnabled: true,
            title:{
                text: titleName + " Progression"
            },
            axisX:{
                valueFormatString:"MMM DD"
            },
            axisY2:{
                valueFormatString:"0",
                maximum: 5.5,
                interval: 1,
                interlacedColor: "#F5F5F5",
                gridColor: "#D7D7D7",
                tickColor: "#D7D7D7"
            },
            theme: "theme2",
            toolTip:{
                shared: true
            },
            legend:{
                verticalAlign: "bottom",
                horizontalAlign: "center",
                fontSize: 15,
                fontFamily: "Lucida Sans Unicode"

            },
            data: [
                {

                    type: "line",
                    lineThickness:3,
                    axisYType:"secondary",
                    showInLegend: true,
                    name: "Attitude",
                    dataPoints: [

                    ]
                },
                {
                    type: "line",
                    lineThickness:3,
                    showInLegend: true,
                    name:  "Attendance",
                    axisYType:"secondary",
                    dataPoints: [
                    ]
                },
                {
                    type: "line",
                    lineThickness:3,
                    showInLegend: true,

                    name: "Ability",
                    axisYType:"secondary",
                    dataPoints: [
                    ]
                }

            ],
            legend: {
                cursor:"pointer",
                itemclick : function(e) {
                    if (typeof(e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
                        e.dataSeries.visible = false;
                    }
                    else {
                        e.dataSeries.visible = true;
                    }
                    chart.render();
                }
            }

        });
        chart.render();

    }

    $("#activateBtn1").click(function () {
        for(var attr = 0; attr < attributes.length; attr++) {
            if(attributes[attr].ageGroup == $("#AttriAgeGroup option:selected").val()
            && attributes[attr].attributeName == $("#AttriGroup option:selected").val()){
                break;
            }
        }

        console.log(attributes[attr].attributeID);
        aja()
        .method("post")
        .url('/api/updateAttribute')
        .queryString({
            attributeID: attributes[attr].attributeID,
            active: 1

        })
        .on('200', function(response){

            $("#evalAttriTableBody").empty()

            aja()
            .method("get")
            .url('/api/getAttributes')
            .queryString({
                clubID: club
            })
            .on('200', function(response){
                setAttributes(response);
            })
            .go();

        })
        .go();



    })

    $("#deactivateBtn2").click(function () {
        for(var attr = 0; attr < attributes.length; attr++) {
            if(attributes[attr].ageGroup == $("#AttriAgeGroup option:selected").val()
            && attributes[attr].attributeName == $("#AttriGroup option:selected").val()){
                break;
            }
        }

        aja()
        .method("post")
        .url('/api/updateAttribute')
        .queryString({
            attributeID: attributes[attr].attributeID,
            active: 0

        })
        .on('200', function(response){

            $("#evalAttriTableBody").empty()

            aja()
            .method("get")
            .url('/api/getAttributes')
            .queryString({
                clubID: club
            })
            .on('200', function(response){
                setAttributes(response);
            })
            .go();
        })
        .go();
    })

    $('#attitudeRange').on('input change', function() {
        $('#attitudeLabel').text(" " + $('#attitudeRange').val());
    });

    $('#abilityRange').on('input change', function() {
        $('#abilityLabel').text(" " + $('#abilityRange').val());
    });

    function addAgeGroupsToGraph() {
        var attributeTableHead = document.getElementById("evalAttriTableHead");
        var attributesHead = document.createElement("th");
        var attributesHeadText = document.createTextNode("Attributes");

        attributesHead.colSpan = "3";
        attributesHead.appendChild(attributesHeadText);
        attributeTableHead.appendChild(attributesHead);


        for(var ah = 0; ah < ageGroups.length; ah++) {
            var tableHead = document.createElement("th");
            var tableHeadText = document.createTextNode(ageGroups[ah][0]);
            console.log(ageGroups[ah][0]);
            tableHead.appendChild(tableHeadText);
            attributeTableHead.appendChild(tableHead);
        }


    }

    function addAttributesToGraph() {

        var attributeTableBody = document.getElementById("evalAttriTableBody");

        for(var ad = 0; ad < 10; ad++) {
            var attrRow = document.createElement("tr");
            var attrTitle = document.createElement("td");
            var attrTitleText = document.createTextNode(attributes[ad].attributeName);
            attrRow.setAttribute("id", attributes[ad].attributeName);
            attrTitle.colSpan = "3";
            attrTitle.appendChild(attrTitleText);
            attrRow.appendChild(attrTitle);
            attributeTableBody.appendChild(attrRow);
        }

        for(var at = 0; at < attributes.length; at++) {
            var row = document.getElementById(attributes[at].attributeName);

            for(var ag = 0; ag < ageGroups.length; ag++) {
                //console.log(ageGroups[ag][0]);
            /*    if(attributes[at].active == 1) {
                    console.log(attributes[at].ageGroup);
                    console.log(attributes[at].ageGroupID);
                    console.log(attributes[at].attributeName);
                    console.log("Active");
                    console.log(ageGroups);
                }*/
                if(attributes[at].ageGroup == ageGroups[ag][0]) {
                    console.log(ageGroups[ag][0]);
                    if(attributes[at].active== 1) {
                        console.log(attributes[at].attributeName);
                        console.log(ageGroups[ag][1] + " FOUND FOUND\n");
                        console.log(ag);
                    }



                    var attrState = document.createElement("td");
                    var attrIcon = document.createElement("i");

                    if(attributes[at].active == 1) {
                        attrIcon.setAttribute("class", "glyphicon glyphicon-ok");
                    } else {
                        attrIcon.setAttribute("class", "glyphicon glyphicon-remove");
                    }
                    attrState.appendChild(attrIcon);
                    row.appendChild(attrState);
                }
            }
        }
    }

}
