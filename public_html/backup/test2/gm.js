$(document).ready(function() {
    function check() {
      if (Cookies.get('email') == null || Cookies.get('type') == null) {
        window.location.href = "index.html";
      } else if (Cookies.get('type') != 4) {
        window.location.href = "index.html";
      }
    }

    check();

    var club = Cookies.get('clubID');
    var account = Cookies.get('email');
    var ageGroups = [];
    var teams = [];
    var attributes = [];


    function addAttributesToGraph() {

        var attributeTableBody = document.getElementById("attriTableBody");

        for(var ad = 0; ad < 10; ad++) {
            var attrRow = document.createElement("tr");
            var attrTitle = document.createElement("td");
            var attrTitleText = document.createTextNode(attributes[ad].attributeName);
            attrRow.setAttribute("id", attributes[ad].attributeName);
            attrTitle.colSpan = "3";
            attrTitle.appendChild(attrTitleText);
            attrRow.appendChild(attrTitle);
            attributeTableBody.appendChild(attrRow);
        }

        for(var at = 0; at < attributes.length; at++) {
            var row = document.getElementById(attributes[at].attributeName);

            for(var ag = 0; ag < ageGroups.length; ag++) {

                if(attributes[at].ageGroupID == ageGroups[ag][1]) {

                    var attrState = document.createElement("td");
                    var attrIcon = document.createElement("i");

                    if(attributes[at].active == 1) {
                        attrIcon.setAttribute("class", "glyphicon glyphicon-ok");
                    } else {
                        attrIcon.setAttribute("class", "glyphicon glyphicon-remove");
                    }
                    attrState.appendChild(attrIcon);
                    row.appendChild(attrState);
                }
            }
        }
    }

    function addAgeGroupsToGraph() {
        var attributeTableHead = document.getElementById("attriTableHead");
        var attributesHead = document.createElement("th");
        var attributesHeadText = document.createTextNode("Attributes");

        attributesHead.colSpan = "3";
        attributesHead.appendChild(attributesHeadText);
        attributeTableHead.appendChild(attributesHead);


        for(var ah = 0; ah < ageGroups.length; ah++) {
            var tableHead = document.createElement("th");
            var tableHeadText = document.createTextNode(ageGroups[ah][0]);

            tableHead.appendChild(tableHeadText);
            attributeTableHead.appendChild(tableHead);
        }


    }

    function setTeamAgeGroup() {
        var teamAgeGroup = document.getElementById("TeamAgeGroupSelect");
        var UpdateAgeGroup = document.getElementById("updateAgeGroupSelect");
        var UpdateTeamAg = document.getElementById("updateTeamAgeGroupSelect");
        var AttriAgeGroup = document.getElementById("AttriAgeGroup");

        for(var ag = 0; ag < ageGroups.length; ag++) {
            var opT = document.createElement("option");
            var opTText = document.createTextNode(ageGroups[ag][0]);

            var opAg = document.createElement("option");
            var opAgText = document.createTextNode(ageGroups[ag][0]);

            var opTAg = document.createElement("option");
            var opTAgText = document.createTextNode(ageGroups[ag][0]);

            var opAttAg = document.createElement("option");
            var opAttAgText = document.createTextNode(ageGroups[ag][0]);

            opT.setAttribute("value", ageGroups[ag][0]);
            opT.appendChild(opTText);

            opAg.setAttribute("value", ageGroups[ag][0]);
            opAg.appendChild(opAgText);

            opTAg.setAttribute("value", ageGroups[ag][0]);
            opTAg.appendChild(opTAgText);

            opAttAg.setAttribute("value", ageGroups[ag][0]);
            opAttAg.appendChild(opAttAgText);

            teamAgeGroup.appendChild(opT);
            UpdateAgeGroup.appendChild(opAg);
            UpdateTeamAg.appendChild(opTAg);
            AttriAgeGroup.appendChild(opAttAg);
        }
        addAgeGroupsToGraph();
    }

    function setTeams() {
        var teamNameSelect = document.getElementById("teamNameSelect");

        for(var tm = 0; tm < teams.length; tm++) {
            var op = document.createElement("option");
            var opText = document.createTextNode(teams[tm][0]);

            op.setAttribute("value", teams[tm][0]);
            op.appendChild(opText);

            teamNameSelect.appendChild(op);
        }

    }

    function setAttributes(attr) {
        attributes = [];
        attributes = attr;
        addAttributesToGraph(attr);
    }

    function setGMClub(clubsInfo) {
        var counter = 0;
        for(var a = 0; a < clubsInfo.length; a++) {

            ageGroups[a] = [];
            ageGroups[a][0] = clubsInfo[a].ageGroup;
            ageGroups[a][1] = clubsInfo[a].ageGroupID;

            for(var b = 0; clubsInfo[a][b] != null; b++) {
                teams[counter] = [];
                teams[counter][0] = clubsInfo[a][b].teamName;
                teams[counter][1] = clubsInfo[a][b].teamID;
                counter++;


            }
        }


        aja()
        .method("get")
        .url('/api/getAttributes')
        .queryString({
            clubID: club
        })
        .on('200', function(response){
            setAttributes(response);
        })
        .go();

        setTeamAgeGroup();
        setTeams();
    }

    function getClubInfo() {
        aja()
        .method("get")
        .url('/api/getGMTeams')
        .queryString({
            email: account
        })
        .on('200', function(response){
            setGMClub(response);
        })
        .go();
    }

    function clearTable() {
        $("#attriTableHead").empty();
        $("#attriTableBody").empty();
        $("#TeamAgeGroupSelect").empty();
        $("#updateAgeGroupSelect").empty();
        $("#updateTeamAgeGroupSelect").empty();
        $("#AttriAgeGroup").empty();
        $("#teamNameSelect").empty();

        attributes = [];
        getClubInfo();
    }

    getClubInfo();

    $(document).on("click", "#submitTeamBtn", function() {
        alert("Team submitted");
    });

    $(document).on("change", ".agcRadio", function() {
        $("#agcSelect").show();
    });

    $(document).on("change", ".tdRadio", function() {
        $("#agcSelect").hide();
    });

    $(document).on('change', ':file', function() {
        var reg = new RegExp(/.*csv$/, 'i');

        var input = $(this);
        var numFiles = input.get(0).files ? input.get(0).files.length : 1;
        var label = input.val().replace(/\\/g, '/').replace(/.*\//, '');


        input.trigger('fileselect', [numFiles, label]);
        $('#fileTitle').val(label);

        if (reg.test(label)){
            console.log("passed");
            $("#fileTitle").removeClass("error").addClass("success");
            $("#errorP").css("display", "none");
            $("#uploadCSV").removeAttr('disabled');
        } else {
            console.log("failed");
            $("#fileTitle").removeClass("success").addClass("error");
            $("#errorP").css("display", "block");
            $("#uploadCSV").attr('disabled', 'disabled');
        }
    });

    function sendCSV(parsedCSV, file) {
        console.log("finished");

        aja()
        .method("post")
        .header('Content-Type', 'application/x-www-form-urlencoded')
        .url('/api/addPlayerCSV')
        .queryString({
            'clubID':club,
            'csv':JSON.stringify(parsedCSV)
        })
        .on('200', function(response) {
            $("#errorP").hide();
            $("#loaderGif").hide();
            $("#loadingMessage").hide();
            $("#successMessage").show();
        })
        .on('404', function(response) {
            $("#loaderGif").hide();
            $("#loadingMessage").hide();
            $("#failureMessage").empty();
            $("#failureMessage").append(response);
            $("#failureMessage").show();
        })
        .on('500', function(response) {
            console.log(response);
        })
        .go();
    }

    $("#uploadCSV").click(function() {
        var csv = $('input[type=file]');
        var fileTitle = $("#fileTitle").val();



        if (!fileTitle) {
            $('#errorP').empty();
            $('#errorP').append("No file chosen");
        } else {
            $("#loaderGif").show();
            $("#loadingMessage").show();
            $("#errorP").hide();
            $("#failureMessage").hide();

            csv.parse({
                config: {
                    complete: sendCSV,
                    header: true
                },
                complete: function() {
                    console.log("complete");
                }
            });
        }
    });

    $("#submitStaffBtn").click(function() {
        //CHecks staff type
        if($("input[name=optradio]:checked").val() == "Age Group Coordinator") {
            //creates a Age Group Coordinator
            aja()
            .method("post")
            .url('/api/addAgeGroup')
            .queryString({
                email: $("#staffEmail").val(),
                clubID: club,
                ageGroup: $("#ageGroupSelect option:selected").val()
            })
            .on('200', function(response){
            })
            .go();

            getClubInfo();
            setGMClub();
            setTeamAgeGroup();

        } else {
            //Creates a TD
            aja()
            .method("post")
            .url('/api/addTD')
            .queryString({
                email: $("#staffEmail").val(),
                clubID: club
            })
            .on('200', function(response){
            })
            .go();
        }
    });

    $("#submitTeamBtn").click(function () {

        aja()
        .method("post")
        .url('/api/addTeam')
        .queryString({
            email: $("#coachEmail").val(),
            clubID: club,
            ageGroup:$("#TeamAgeGroupSelect").val(),
            teamName: $("#teamName").val(),
            coachFirstName: $("#coachFirstName").val(),
            coachLastName: $("#coachLastName").val(),
            managerEmail: $("#assistCoachEmail").val(),
            assCoachEmail: $("#managerEmail").val()
        })
        .on('200', function(response){
        })
        .go();

    })

    $("#updateAgeGroupBtn").click(function () {
        for(var i = 0; i < ageGroups; i++) {
            if(ageGroups[i] == $("#updateAgeGroupSelect option:selected").val()) {
                break;
            }
        }
        aja()
        .method("post")
        .url('/api/updateAgeGroup')
        .queryString({
            ageGroupID: ageGroups[i][1],
            ageGroup: $("#updateAgeGroupSelect option:selected").val(),
            email: $("#ageCoordEmail").val()
        })
        .on('200', function(response){
            console.log("hey it worked");
        })
        .go();
    })

    $("#updateTeamBtn").click(function () {

        for(var k = 0; k < ageGroups.length; k++) {
            if(ageGroups[k][0] == $("#updateTeamAgeGroupSelect option:selected").val()) {
                break;
            }
        }

        for(var j = 0; j < teams.length; j++) {
            if(teams[j][0] == $("#teamNameSelect option:selected").val()) {
                break;
            }
        }

        aja()
        .method("post")
        .url('/api/updateTeam')
        .queryString({
            email: $("#UpdateCoachEmail").val(),
            ageGroupID: ageGroups[k][1],
            teamID: teams[j][1],
            teamName: $("#teamNameSelect").val(),
            coachFirstName: $("#UpdateCoachFirstName").val(),
            coachLastName: $("#UpdateCoachLastName").val(),
            managerEmail: $("#UpdateAssistCoachEmail").val(),
            assCoachEmail: $("#UpdateManagerEmail").val()
        })
        .on('200', function(response){
        })
        .go();

    })

    $("#activateBtn").click(function () {
        for(var attr = 0; attr < attributes.length; attr++) {
            if(attributes[attr].ageGroup == $("#AttriAgeGroup option:selected").val()
            && attributes[attr].attributeName == $("#AttriGroup option:selected").val()){
                console.log("FOUND FOUND FOUND");
                break;
            }
        }

        aja()
        .method("post")
        .url('/api/updateAttribute')
        .queryString({
            attributeID: attributes[attr].attributeID,
            active: 1

        })
        .on('200', function(response){
            clearTable();
        })
        .on('404', function(response){
            console.log("could not activate attribute");
        })
        .go();



    })

    $("#deactivateBtn").click(function () {

        for(var attr = 0; attr < attributes.length; attr++) {
            if(attributes[attr].ageGroup == $("#AttriAgeGroup option:selected").val()
            && attributes[attr].attributeName == $("#AttriGroup option:selected").val()){
                break;
            }
        }

        aja()
        .method("post")
        .url('/api/updateAttribute')
        .queryString({
            attributeID: attributes[attr].attributeID,
            active: 0

        })
        .on('200', function(response){
            clearTable();
        })
        .on('404', function(response){
            console.log("could not deactivate attribute");
        })
        .go();
    })

    $("#sendEmails").click(function() {
        aja()
        .method("get")
        .url('/api/sendEmails')
        .queryString({
            clubID: club
        })
        .on('200', function(response){
        })
        .on('404', function(response){
        })
        .go();
    })

    $("#ageGroupCheck").change(function() {

      $("#loaderGifSide").show();

      aja()
      .method("get")
      .url('/api/getAgeGroupTeams')
      .queryString({
        ageGroupID: //FIX
      })
      .on('200', function(response){

      })
      .on('404', function(response){

      })
      .go();
    })
});
