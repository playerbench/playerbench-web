$(document).ready(function() {
	$("#submitBtn").click(function() {
				//do something when clicked
				var eventID = $("#eventID").val();
				var playerID = parseInt($("#playerID").val());
				var attendance = parseInt($("#attendance").val());
				var attendanceComment = $("#attendanceComment").val();
				var attitude = parseInt($("#attitude").val());
				var attitudeComment = $("#attitudeComment").val();
				var ability = parseInt($("#ability").val());
				aja()
				.method("post")
				.url('/api/submitEvaluation')
				.queryString({
					'eventID': 1,
					'playerID': 5,
					'attendance': attendance ,
					'attendanceComment': attendanceComment ,
					'attitude': attitude ,
					'attitudeComment': attitudeComment ,
					'ability': ability
				})
				.on('200', function(response){
					console.log("Success");
				})
				.on('404', function(response){
					console.log("Evaluation not submited");
				})
				.go();
		});
	});