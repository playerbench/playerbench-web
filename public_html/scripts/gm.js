$(document).ready(function() {
    function check() {
      if (Cookies.get('email') == null || Cookies.get('type') == null) {
        window.location.href = "index.html";
      } else if (Cookies.get('type') != 4) {
        window.location.href = "index.html";
      }
    }

    check();

    var club = Cookies.get('clubID');
    var account = Cookies.get('email');
    var ageGroups = [];
    var teams = [];
    var attributes = [];

    if (Cookies.get('gmMode') == 'maintain') {
        $("#modeTell").text("You are in maintenance mode");
        getMode();
    } else {
        $("#modeTell").text("You are in initialization mode");
        getMode();
    }

    function addAttributesToGraph() {

        var attributeTableBody = document.getElementById("attriTableBody");

        for(var ad = 0; ad < 10; ad++) {
            var attrRow = document.createElement("tr");
            var attrTitle = document.createElement("td");
            var attrTitleText = document.createTextNode(attributes[ad].attributeName);
            attrRow.setAttribute("id", attributes[ad].attributeName);
            attrTitle.colSpan = "3";
            attrTitle.appendChild(attrTitleText);
            attrRow.appendChild(attrTitle);
            attributeTableBody.appendChild(attrRow);
        }

        for(var at = 0; at < attributes.length; at++) {
            var row = document.getElementById(attributes[at].attributeName);

            for(var ag = 0; ag < ageGroups.length; ag++) {

                if(attributes[at].ageGroupID == ageGroups[ag][1]) {

                    var attrState = document.createElement("td");
                    var attrIcon = document.createElement("i");

                    if(attributes[at].active == 1) {
                        attrIcon.setAttribute("class", "glyphicon glyphicon-ok");
                    } else {
                        attrIcon.setAttribute("class", "glyphicon glyphicon-remove");
                    }
                    attrState.appendChild(attrIcon);
                    row.appendChild(attrState);
                }
            }
        }
    }

    function addAgeGroupsToGraph() {
        var attributeTableHead = document.getElementById("attriTableHead");
        var attributesHead = document.createElement("th");
        var attributesHeadText = document.createTextNode("Attributes");

        attributesHead.colSpan = "3";
        attributesHead.appendChild(attributesHeadText);
        attributeTableHead.appendChild(attributesHead);


        for(var ah = 0; ah < ageGroups.length; ah++) {
            var tableHead = document.createElement("th");
            var tableHeadText = document.createTextNode(ageGroups[ah][0]);

            tableHead.appendChild(tableHeadText);
            attributeTableHead.appendChild(tableHead);
        }


    }

    function setTeamAgeGroup() {
        var teamAgeGroup = document.getElementById("TeamAgeGroupSelect");
        var UpdateAgeGroup = document.getElementById("updateAgeGroupSelect");
        var UpdateTeamAg = document.getElementById("updateTeamAgeGroupSelect");
        var AttriAgeGroup = document.getElementById("AttriAgeGroup");
        var ageGroupCheck = document.getElementById("ageGroupCheck");

        for(var ag = 0; ag < ageGroups.length; ag++) {
            var opT = document.createElement("option");
            var opTText = document.createTextNode(ageGroups[ag][0]);

            var opAg = document.createElement("option");
            var opAgText = document.createTextNode(ageGroups[ag][0]);

            var opTAg = document.createElement("option");
            var opTAgText = document.createTextNode(ageGroups[ag][0]);

            var opAttAg = document.createElement("option");
            var opAttAgText = document.createTextNode(ageGroups[ag][0]);

            var agc = document.createElement("option");
            var agcText = document.createTextNode(ageGroups[ag][0]);

            opT.setAttribute("value", ageGroups[ag][0]);
            opT.appendChild(opTText);

            opAg.setAttribute("value", ageGroups[ag][0]);
            opAg.appendChild(opAgText);

            opTAg.setAttribute("value", ageGroups[ag][0]);
            opTAg.appendChild(opTAgText);

            opAttAg.setAttribute("value", ageGroups[ag][0]);
            opAttAg.appendChild(opAttAgText);

            agc.setAttribute("value", ageGroups[ag][1]);
            agc.appendChild(agcText);

            //teamAgeGroup.appendChild(opT);
            UpdateAgeGroup.appendChild(opAg);
            UpdateTeamAg.appendChild(opTAg);
            AttriAgeGroup.appendChild(opAttAg);

            ageGroupCheck.appendChild(agc);
        }
        addAgeGroupsToGraph();
    }

    function setTeams() {
        var teamNameSelect = document.getElementById("teamNameSelect");

        for(var tm = 0; tm < teams.length; tm++) {
            var op = document.createElement("option");
            var opText = document.createTextNode(teams[tm][0]);

            op.setAttribute("value", teams[tm][0]);
            op.appendChild(opText);

            teamNameSelect.appendChild(op);
        }

    }

    function setAttributes(attr) {
        attributes = [];
        attributes = attr;
        addAttributesToGraph(attr);
    }

    function setGMClub(clubsInfo) {
        var counter = 0;
        for(var a = 0; a < clubsInfo.length; a++) {

            ageGroups[a] = [];
            ageGroups[a][0] = clubsInfo[a].ageGroup;
            ageGroups[a][1] = clubsInfo[a].ageGroupID;

            for(var b = 0; clubsInfo[a][b] != null; b++) {
                teams[counter] = [];
                teams[counter][0] = clubsInfo[a][b].teamName;
                teams[counter][1] = clubsInfo[a][b].teamID;
                counter++;
            }
        }

        aja()
        .method("get")
        .url('/api/getAttributes')
        .queryString({
            clubID: club
        })
        .on('200', function(response){
            setAttributes(response);
        })
        .go();

        setTeamAgeGroup();
        setTeams();
    }

    function getClubInfo() {
        aja()
        .method("get")
        .url('/api/getGMTeams')
        .queryString({
            email: account
        })
        .on('200', function(response){
            setGMClub(response);
        })
        .go();
    }

    function clearTable() {
        $("#attriTableHead").empty();
        $("#attriTableBody").empty();
        $("#TeamAgeGroupSelect").empty();
        $("#updateAgeGroupSelect").empty();
        $("#updateTeamAgeGroupSelect").empty();
        $("#AttriAgeGroup").empty();
        $("#teamNameSelect").empty();

        attributes = [];
        getClubInfo();
    }

    getClubInfo();

    $(document).on("click", "#submitTeamBtn", function() {
    });

    $(document).on("change", ".agcRadio", function() {
        $("#agcSelect").show();
    });

    $(document).on("change", ".tdRadio", function() {
        $("#agcSelect").hide();
    });

    $(document).on('change', ':file', function() {
        var reg = new RegExp(/.*csv$/, 'i');

        var input = $(this);
        var numFiles = input.get(0).files ? input.get(0).files.length : 1;
        var label = input.val().replace(/\\/g, '/').replace(/.*\//, '');


        input.trigger('fileselect', [numFiles, label]);
        $('#fileTitle').val(label);

        if (reg.test(label)){
            $("#fileTitle").removeClass("error").addClass("success");
            $("#errorP").css("display", "none");
            $("#uploadCSV").removeAttr('disabled');
        } else {
            $("#fileTitle").removeClass("success").addClass("error");
            $("#errorP").css("display", "block");
            $("#uploadCSV").attr('disabled', 'disabled');
        }
    });

    function sendCSV(parsedCSV, file) {
      var csvFile = JSON.stringify(parsedCSV);
      console.log(csvFile);

      if ($("input[name=csvradio]:checked").val() == "Team") {

        aja()
        .method("post")
        .header('Content-Type', 'application/x-www-form-urlencoded')
        .url('/api/addTeamsCSV')
        .body({clubID:club, csv:csvFile})
        // .queryString({
        //     'clubID':club,
        //     'csv':JSON.stringify(parsedCSV)
        // })
        .on('200', function(response) {
            $("#errorP").hide();
            $("#loaderGif").hide();
            $("#loadingMessage").hide();
            $("#successMessage").show();
        })
        .on('404', function(response) {
            $("#loaderGif").hide();
            $("#loadingMessage").hide();
            $("#failureMessage").empty();
            $("#failureMessage").append(response);
            $("#failureMessage").show();
        })
        .on('500', function(response) {
            console.log(response);
        })
        .go();
      }

      else if ($("input[name=csvradio]:checked").val() == "Roster") {

        aja()
        .method("post")
        .header('Content-Type', 'application/x-www-form-urlencoded')
        .url('/api/addPlayerCSV')
        .body({club, csvFile})
        // .queryString({
        //     'clubID':club,
        //     'csv':JSON.stringify(parsedCSV)
        // })
        .on('200', function(response) {
            $("#errorP").hide();
            $("#loaderGif").hide();
            $("#loadingMessage").hide();
            $("#successMessage").show();
        })
        .on('404', function(response) {
            $("#loaderGif").hide();
            $("#loadingMessage").hide();
            $("#failureMessage").empty();
            $("#failureMessage").append(response);
            $("#failureMessage").show();
        })
        .on('500', function(response) {
            console.log(response);
        })
        .go();
      }

      else {
        $("#loaderGif").hide();
        $("#loadingMessage").hide();
        $("#failureMessage").empty();
        $("#failureMessage").append("Error. Check if option is selected.");
        $("#failureMessage").show();
      }
    }

    $("#uploadCSV").click(function() {
        var csv = $('input[type=file]');
        var fileTitle = $("#fileTitle").val();

        if (!fileTitle) {
        } else {
            $("#loaderGif").show();
            $("#loadingMessage").show();
            $("#errorP").hide();
            $("#failureMessage").hide();

            csv.parse({
                config: {
                    complete: sendCSV,
                    skipEmtyLines: true,
                    header: true
                },
                complete: function() {
                    console.log("complete");
                }
            });
        }
    });

    $("#submitStaffBtn").click(function() {

        $("#loaderGifStaff").show();
        //Checks staff type
        if($("input[name=optradio]:checked").val() == "Age Group Coordinator") {

            var gender = $("input[name=genderRadio]:checked").val();

            var newAgeGroup = $("#ageGroupSelect option:selected").val() + gender;


            //Hide the error message
            $("#failureMessageStaff").hide();

            //creates a Age Group Coordinator
            aja()
            .method("post")
            .url('/api/addAgeGroup')
            .queryString({
                email: $("#staffEmail").val(),
                clubID: club,
                ageGroup: newAgeGroup
            })
            .on('200', function(response){
                $("#loaderGifStaff").hide();
                $("failureMessageStaff").hide();
                $("#successMessageStaff").show();
                setTimeout(function() {
                    location.reload();
                }, 500);
            })
            .on('404', function(response){
                $("#loaderGifStaff").hide();
                $("failureMessageStaff").empty();
                $("failureMessageStaff").append(response);
                $("failureMessageStaff").show();
            })
            .on('500', function(response){
                $("#loaderGifStaff").hide();
                $("failureMessageStaff").empty();
                $("failureMessageStaff").append("Server Error. Please Try Again.");
                $("failureMessageStaff").show();
            })
            .go();

            getClubInfo();
            setGMClub();
            setTeamAgeGroup();

        } else if($("input[name=optradio]:checked").val() == "Technical Director") {

            //Hide the error message
            $("#failureMessageStaff").hide();

            //Creates a TD
            aja()
            .method("post")
            .url('/api/addTD')
            .queryString({
                email: $("#staffEmail").val(),
                clubID: club
            })
            .on('200', function(response){
                $("#loaderGifStaff").hide();
                $("#successMessageStaff").show();
            })
            .on('404', function(response){
                $("#loaderGifStaff").hide();
                $("failureMessageStaff").empty();
                $("failureMessageStaff").append(response);
                $("failureMessageStaff").show();
            })
            .on('500', function(response){
                $("#loaderGifStaff").hide();
                $("failureMessageStaff").empty();
                $("failureMessageStaff").append("Server Error. Please Try Again.");
                $("failureMessageStaff").show();
            })
            .go();

        } else { //Nothing was chosen as a staff member
            $("#loaderGifStaff").hide();
            $("#failureMessageStaff").empty();
            $("#failureMessageStaff").append("No Option Selected");
            $("#failureMessageStaff").show();
        }
    });

    $("#updateAgeGroupBtn").click(function () {

        if (!($("#ageCoordEmail").val())) {
            $("#failureMessageAgeGroup").empty();
            $("#failureMessageAgeGroup").append("No Email Entered");
            $("#failureMessageAgeGroup").show();

        } else {
            $("#loaderGifAgeGroup").show();
            $("#failureMessageAgeGroup").hide();
            $("#successMessageAgeGroup").hide();

            for(var i = 0; i < ageGroups; i++) {
                if(ageGroups[i] == $("#updateAgeGroupSelect option:selected").val()) {
                    break;
                }
            }

            aja()
            .method("post")
            .url('/api/updateAgeGroup')
            .queryString({
                ageGroupID: ageGroups[i][1],
                ageGroup: $("#updateAgeGroupSelect option:selected").val(),
                email: $("#ageCoordEmail").val()
            })
            .on('200', function(response){
                $("#loaderGifAgeGroup").hide();
                $("#successMessageAgeGroup").show();
            })
            .on('404', function(response){
                $("#loaderGifAgeGroup").hide();
                $("#failureMessageAgeGroup").empty();
                $("#failureMessageAgeGroup").append(response);
                $("#failureMessageAgeGroup").show();
            })
            .on('500', function(response){
                $("#loaderGifAgeGroup").hide();
                $("#failureMessageAgeGroup").empty();
                $("#failureMessageAgeGroup").append("Error. Please Try Again.");
                $("#failureMessageAgeGroup").show();
            })
            .go();
        }
    })

    $("#updateTeamBtn").click(function () {

        if (!($("#UpdateCoachFirstName")) || !($("#UpdateCoachLastName")) || !($("#UpdateCoachEmail"))) {
            $("#failureMessageTeam").empty();
            $("#failureMessageTeam").append("Data Incomplete. One or more text fields are blank.");
            $("#failureMessageTeam").show();

        } else {
            $("#loaderGifTeam").show();
            $("#failureMessageTeam").hide();
            $("#successMessageTeam").hide();

            for(var k = 0; k < ageGroups.length; k++) {
                if(ageGroups[k][0] == $("#updateTeamAgeGroupSelect option:selected").val()) {
                    break;
                }
            }

            for(var j = 0; j < teams.length; j++) {
                if(teams[j][0] == $("#teamNameSelect option:selected").val()) {
                    break;
                }
            }

            aja()
            .method("post")
            .url('/api/updateTeam')
            .queryString({
                email: $("#UpdateCoachEmail").val(),
                ageGroupID: ageGroups[k][1],
                teamID: teams[j][1],
                teamName: $("#teamNameSelect").val(),
                coachFirstName: $("#UpdateCoachFirstName").val(),
                coachLastName: $("#UpdateCoachLastName").val(),
                managerEmail: $("#UpdateAssistCoachEmail").val(),
                assCoachEmail: $("#UpdateManagerEmail").val()
            })
            .on('200', function(response){
                $("#loaderGifTeam").hide();
                $("#successMessageTeam").show();
            })
            .on('404', function(response){
                $("#loaderGifTeam").hide();
                $("#failureMessageTeam").empty();
                $("#failureMessageTeam").append(response);
                $("#failureMessageTeam").empty();
            })
            .on('500', function(response){
                $("#loaderGifTeam").hide();
                $("#failureMessageTeam").empty();
                $("#failureMessageTeam").append("Error. Please Try Again.");
                $("#failureMessageTeam").empty();
            })
            .go();
        }

    })

    $("#activateBtn").click(function () {
        for(var attr = 0; attr < attributes.length; attr++) {
            if(attributes[attr].ageGroup == $("#AttriAgeGroup option:selected").val()
            && attributes[attr].attributeName == $("#AttriGroup option:selected").val()){
                console.log("FOUND FOUND FOUND");
                break;
            }
        }

        aja()
        .method("post")
        .url('/api/updateAttribute')
        .queryString({
            attributeID: attributes[attr].attributeID,
            active: 1

        })
        .on('200', function(response){
            clearTable();
        })
        .on('404', function(response){
            console.log("could not activate attribute");
        })
        .go();



    })

    $("#deactivateBtn").click(function () {

        for(var attr = 0; attr < attributes.length; attr++) {
            if(attributes[attr].ageGroup == $("#AttriAgeGroup option:selected").val()
            && attributes[attr].attributeName == $("#AttriGroup option:selected").val()){
                break;
            }
        }

        aja()
        .method("post")
        .url('/api/updateAttribute')
        .queryString({
            attributeID: attributes[attr].attributeID,
            active: 0

        })
        .on('200', function(response){
            clearTable();
        })
        .on('404', function(response){
            console.log("could not deactivate attribute");
        })
        .go();
    })

    $("#sendEmails").click(function() {
        $("#loaderGifEmail").show();
        $("#successMessageEmail").hide();
        $("#failureMessageEmail").hide();

        aja()
        .method("get")
        .url('/api/sendEmails')
        .queryString({
            clubID: club
        })
        .on('200', function(response){
            $("#loaderGifEmail").hide();
            $("#successMessageEmail").show();
        })
        .on('404', function(response){
            $("#loaderGifEmail").hide();
            $("#failureMessageEmail").empty();
            $("#failureMessageEmail").append(response);
            $("#failureMessageEmail").show();
        })
        .on('500', function(response){
            console.log(response);
            $("#loaderGifEmail").hide();
            $("#failureMessageEmail").empty();
            $("#failureMessageEmail").append("Error. Please Try Again.");
            $("#failureMessageEmail").show();
        })
        .go();
    })

    function createTeamList(response) {
      for (var x = 0; x < response.length; x++) {
        var list = document.getElementById("teamList");
        var listItem = document.createElement("li");
        var teamName = document.createTextNode(response[x].teamName);

        listItem.appendChild(teamName);
        list.appendChild(listItem);
      }
    }

    $(document).on("change", "#ageGroupCheck", function() {
      $("#loaderGifSide").show();
      $("#errorSideMessage").hide();
      $("#teamList").empty();

      aja()
      .method("get")
      .url('/api/getAgeGroupTeams')
      .queryString({
        ageGroupID: $("#ageGroupCheck").val()
      })
      .on('200', function(response){
        $("#loaderGifSide").hide();
        $("#errorSideMessage").hide();
        createTeamList(response);
      })

      .on('404', function(response){
        $("#loaderGifSide").hide();
        $("#errorSideMessage").show();
      })
      .go();
    })

    function getMode() {
        if (Cookies.get('gmMode') == 'maintain') {
            $(".addStaffTitle").text("Add Staff");
            $(".updateAGTitle").text("Update Age Group Coordinator");
            $(".csvTitle").text("Upload CSV");
            $(".updateTeamTitle").text("Update Team");
            $(".emailTitle").text("Activation Emails");
            $(".attributeTitle").text("Set Attributes");

            $("#updateAGDiv").show();
            $("#updateTeamDiv").hide();

        } else {
            $("#updateAGDiv").hide();
            $("#updateTeamDiv").hide();

            $(".addStaffTitle").text("Step #1: Add Staff Members");
            $(".csvTitle").text("Step #2: Upload CSV");
            $(".emailTitle").text("Step #3: Activation Emails");
            $(".attributeTitle").text("Step #4: Set Attributes");
        }
    }

    $("#changeModeBtn").click(function() {
        if (Cookies.get('gmMode') == 'init') {
            Cookies.set('gmMode', 'maintain');
            $("#modeTell").empty();
            $("#modeTell").text("You are in maintenance mode");
            getMode();
        } else {
            Cookies.set('gmMode', 'init');
            $("#modeTell").empty();
            $("#modeTell").text("You are in initialization mode");
            getMode();
        }

    })
});
