$(document).ready(function() {
  $('#registerBtn').click(function(event) {
    //Get user input from fields
    var firstName = $('#regFN').val();
    var lastName = $('#regLN').val();
    var email = $('#regEmail').val();
    var password = $('#regPassword').val();
    var confirmPassword = $('#regConfirmPassword').val();

    var errorMessage = $('#invalidMsg');
    var errorIcon = "<i class=\"glyphicon glyphicon-alert\"></i><br />"

    //Getting the clubID from the URL
    var url = window.location.href;

    var split = url.indexOf('?');
    var start = split + 18;
    var clubIDStr = "";

    while(url[start] != 'h') {
      clubIDStr += url[start];
      start++;
    }

    var clubID = parseInt(clubIDStr);

    if (!password || !confirmPassword || !firstName || !lastName || !email) {
      errorMessage.empty();
      errorMessage.append(errorIcon + "Empty Field(s)").show();
    } else if (password != confirmPassword) {
      errorMessage.empty();
      errorMessage.append(errorIcon + "Passwords Do Not Match").show();
    } else if (password == confirmPassword){
      errorMessage.hide();
      $('#loaderGif').show();

      aja()
        .method('post')
        .url('/api/addUser')
        .header('Content-Type','application/x-www-form-urlencoded')
        .queryString({
          'email':email,
          'clubID':clubID,
          'password':password,
          'firstName':firstName,
          'lastName':lastName,
        })
        .on('200', function(response){
          window.location = "http://www.soccer-pro-file.com/index.html"
        })
        .on('409', function(response) {
          $('#loaderGif').hide();
          errorMessage.empty();
          errorMessage.append(errorIcon + "Email Already Registered.").show();
        })
        .on('404', function(response) {
          getError(response);
        })
        .go();

    } else {
      errorMessage.empty();
      errorMessage.append(errorIcon + "Unexpected Error Occurred.").show();
    }
  });

  function getError(response) {
    var errorMessage = $('#invalidMsg');
    var errorIcon = "<i class=\"glyphicon glyphicon-alert\"></i><br />"

    var error = response[41];

    if (error == "0") {
      $('#loaderGif').hide();
      errorMessage.empty();
      errorMessage.append(errorIcon + "Something Went Wrong. Try Again. If this problem persists, please contact Soccer Pro-File support.").show();
    } else if (error == "1") {
      $('#loaderGif').hide();
      errorMessage.empty();
      errorMessage.append(errorIcon + "Email not part of a club. Please contact the club General Manager.").show();
    }
  };
});
