$(document).ready(function() {
    console.log("hey hey hey");
    $('#gmRegisterBtn').click(function() {
        var email = $('#gmRegEmail').val();
        var firstName = $('#gmRegFN').val();
        var lastName =  $('#gmRegLN').val();
        var clubName = $('#gmRegCN').val();
        var userPassword = $('#gmRegPassword').val();
        var confirmUserPassword =  $('#gmRegConfirmPassword').val();
        var password = $('#appPassword').val();

        var errorMessage = $('#gmInvalidMsg');
        var errorIcon = "<i class=\"glyphicon glyphicon-alert\"></i><br />"

        if (!userPassword || !confirmUserPassword || !firstName || !lastName || !email || !password) {

            errorMessage.empty();
            errorMessage.append(errorIcon + "Empty Field(s)").show();
        } else if (userPassword != confirmUserPassword) {

            errorMessage.empty();
            errorMessage.append(errorIcon + "Passwords Do Not Match").show();
        } else if (userPassword == confirmUserPassword){
            errorMessage.hide();
            $('#loaderGif').show();

            aja()
            .method('post')
            .url('/api/addGM')
            .header('Content-Type','application/x-www-form-urlencoded')
            .queryString({
                'email':email,
                'activationPassword':password,
                'password' : userPassword,
                'firstName':firstName,
                'lastName':lastName,
                'clubName':clubName,
            })
            .on('200', function(response){
                window.location = "http://www.soccer-pro-file.com/index.html"
            })
            .on('409', function(response) {
                $('#loaderGif').hide();
                errorMessage.empty();
                errorMessage.append(errorIcon + "Email Already Registered.").show();
            })
            .on('404', function(response) {
                $('#loaderGif').hide();
                getError(response);
            })
            .go();

        } else {
            errorMessage.empty();
            errorMessage.append(errorIcon + "Unexpected Error Occurred.").show();
        }
    });

})
