function Email(address) {
  var email = address;

  function getEmailAddress() {
    return email;
  }

  function setEmailAddress(newAddress) {
    email = newAddress;
  }
}

function Account(type) {
  var acctType = type;

  function getAccountType() {
    return acctType;
  }

  function setAccountType(newType) {
    acctType = newType;
  }
}
