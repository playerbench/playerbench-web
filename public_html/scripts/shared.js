//var populate = function()
//set child ID

window.onload = function() {
    $('#aboutLH').popover();
    $('#aboutT').popover();
    $('#aboutA').popover();
    $('#aboutC').popover();
    $('#aboutCH').popover();
    $('#aboutCl').popover();

    //request code taken from http://stackoverflow.com/questions/9713058/sending-post-data-with-a-xmlhttprequest
    var selectedPlayer = 0;
    var playerID = 0;
    var player = new Player(0);
    var firstName;
    var lastName;

    var fnameList = [];
    var lnameList = [];
    var idList = [];
    var nameSelected;

    var playerCV = [];

    var link = Cookies.get('shareLink');

    console.log(Cookies.get('shareLink'));

    //get's list of parent's children
    function setName(playerInfo) {
        firstName = playerInfo.firstName
        lastName = playerInfo.lastName
        getCV(playerInfo);
    }

    function getCV(cvInfo) {
        playerCV = [];
        for(var i = 0; i < cvInfo.length; i++) {
            playerCV[i] = cvInfo[i];
        }
        populateCV(cvInfo);
    }

    console.log(link);
    aja()
    .method("get")
    .url('/api/getCV/shared')
    .queryString({
        'link': link
    })
    .on('200', function(response){
        playerID = response.playerID;
        console.log(response)
        setName(response);
    })
    .go();



    sideMenu();

    function sideMenu() {

        //sidePanel
        var sidePanel= document.getElementById("sidePanel");
        //panel-group
        var panelGroup = document.createElement("div");



        panelGroup.setAttribute("class", "panel-group");
        panelGroup.setAttribute("id", "accordion");
        //add panelGroup to side panel
        sidePanel.appendChild(panelGroup);

        var panelBody = document.createElement("div");
        var table = document.createElement("table");
        var tableBody = document.createElement("tbody");

        var leagueRow = document.createElement("tr");
        var leagueData = document.createElement("td");
        var leagueButton = document.createElement("a");
        var leagueH = document.createTextNode("League History");

        var tournamentsRow = document.createElement("tr");
        var tournamentsData = document.createElement("td");
        var tournamentsButton = document.createElement("a");
        var tournamentsH = document.createTextNode("Tournaments");

        var academiesRow = document.createElement("tr");
        var academiesData = document.createElement("td");
        var academiesButton = document.createElement("a");
        var academiesH = document.createTextNode("Academies");

        var campsRow = document.createElement("tr");
        var campsData = document.createElement("td");
        var campsButton = document.createElement("a");
        var campsH = document.createTextNode("Camps");

        var compRow = document.createElement("tr");
        var compData = document.createElement("td");
        var compButton = document.createElement("a");
        var compH = document.createTextNode("Competition History");

        var clubRow = document.createElement("tr");
        var clubData = document.createElement("td");
        var clubButton = document.createElement("a");
        var clubH = document.createTextNode("Clubs");

        panelGroup.appendChild(panelBody);
        //table in oanel body
        table.setAttribute("class", "table table-condensed bg-danger");
        table.setAttribute("id", "sharedTable");

        //adds table to panel body
        panelBody.appendChild(table);

        //League history
        leagueButton.setAttribute("class", "page-scroll");
        leagueButton.setAttribute("href", "#leagueHistory")

        leagueButton.appendChild(leagueH);
        leagueData.appendChild(leagueButton);
        leagueRow.appendChild(leagueData);
        tableBody.appendChild(leagueRow);

        //tournaments
        tournamentsButton.setAttribute("class", "page-scroll");
        tournamentsButton.setAttribute("href", "#tournaments")

        tournamentsButton.appendChild(tournamentsH);
        tournamentsData.appendChild(tournamentsButton);
        tournamentsRow.appendChild(tournamentsData);
        tableBody.appendChild(tournamentsRow);

        //academies
        academiesButton.setAttribute("class", "page-scroll");
        academiesButton.setAttribute("href", "#academies")

        academiesButton.appendChild(academiesH);
        academiesData.appendChild(academiesButton);
        academiesRow.appendChild(academiesData);
        tableBody.appendChild(academiesRow);

        //camps
        campsButton.setAttribute("class", "page-scroll");
        campsButton.setAttribute("href", "#camps")

        campsButton.appendChild(campsH);
        campsData.appendChild(campsButton);
        campsRow.appendChild(campsData);
        tableBody.appendChild(campsRow);

        //Competition History
        compButton.setAttribute("class", "page-scroll");
        compButton.setAttribute("href", "#compHistory");

        compButton.appendChild(compH);
        compData.appendChild(compButton);
        compRow.appendChild(compData);
        tableBody.appendChild(compRow);

        //Clubs
        clubButton.setAttribute("class", "page-scroll");
        clubButton.setAttribute("href", "#clubs")

        clubButton.appendChild(clubH);
        clubData.appendChild(clubButton);
        clubRow.appendChild(clubData);
        tableBody.appendChild(clubRow);

        table.appendChild(tableBody);
    }

    function getAge(dOB) {
        var dateOfBirth = new Date(dOB);
        var today = new Date();
        var age = today.getFullYear() - dateOfBirth.getFullYear();
        var m = today.getMonth() - dateOfBirth.getMonth();
        if (m < 0 || (m === 0 && today.getDate() < dateOfBirth.getDate())) {
            age--;
        }
        return age;
    }

    function populateCV(playerCV) {
        var index;
        for(index = 0; index < idList.length; index++) {
            if(playerID == idList[index]) {
                break;
            }
        }
        console.log(playerCV);
        var age = getAge(playerCV.dateOfBirth)

        //Basic information table
        $("#firstName").text(firstName);
        $("#lastName").text(lastName);
        $("#nationality").text(playerCV.nationality);
        $("#birthYear").text(playerCV.dateOfBirth);
        $("#age").text(age);
        $("#ageGroup").text(playerCV.ageGroup);
        $("#club").text(playerCV.clubName);
        $("#team").text(playerCV.teamName);
        $("#coach").text(playerCV.coachFirstName + " " + playerCV.coachLastName);


        for(var i = 0; i < playerCV[i] != null; i++) {
            var eve = " ";
            var image  = document.createElement("img");

            var nameLabel;
            var dateLabel;
            var infoLabel;
            var cityLabel = "City: ";

            var format;
            var variable;

            if(playerCV[i].eventType == "Tournaments") {

                eve = document.getElementById("tournamentsBody");
                nameLabel = "Tournament Name: ";
                dateLabel = "Date: ";
                infoLabel = "Final Placing: ";

                image.setAttribute("src", "http://image.flaticon.com/icons/svg/53/53283.svg");
                image.style.height = "50px";

            } else if(playerCV[i].eventType == "League History") {

                format = 1;
                variable = 1;

                eve = document.getElementById("leagueHistoryBody");
                nameLabel = "League Name: ";
                dateLabel = "Year: ";
                infoLabel = "Club Name: ";
                image.setAttribute("src", "https://d30y9cdsu7xlg0.cloudfront.net/png/16635-200.png");
                image.style.height = "60px";

            } else if(playerCV[i].eventType == "Academies") {

                format = 1;
                variable = 1;

                eve = document.getElementById("academiesBody");
                nameLabel = "Academy Name: ";
                dateLabel = "Year: ";
                infoLabel = "Coach Name: ";
                image.setAttribute("src", "http://image.flaticon.com/icons/svg/53/53283.svg");
                image.style.height = "6%";

            } else if(playerCV[i].eventType == "Camps") {


                format = 2;
                variable = 0;

                eve = document.getElementById("campsBody");
                nameLabel = "Camp Name: ";
                dateLabel = "Date: ";
                infoLabel = "Team Name: ";
                image.setAttribute("src", "http://image.flaticon.com/icons/svg/53/53283.svg");
                image.style.height = "6%";

            } else if(playerCV[i].eventType == "Competition History") {

                format = 2;
                variable = 0;

                eve = document.getElementById("compHistoryBody");
                nameLabel = "Competition Name: ";
                dateLabel = "Date: ";
                infoLabel = "Final Placing: ";
                image.setAttribute("src", "http://image.flaticon.com/icons/svg/53/53283.svg");
                image.style.height = "6%";

            }  else if(playerCV[i].eventType == "Clubs") {

                format = 1;
                variable = 1;

                eve = document.getElementById("clubsBody");
                nameLabel = "Club Name: ";
                dateLabel = "Year: ";
                infoLabel = "Coach Name: ";
                image.setAttribute("src", "http://image.flaticon.com/icons/svg/53/53283.svg");
                image.style.height = "6%";

            } else {
                break;
            }


            var dateSplit = (playerCV[i].eventDate).split("-");

            var month = " ";

            if (dateSplit[1] == 1) {
                month = "JAN";
            } else if (dateSplit[1] == 2) {
                month = "FEB";
            } else if (dateSplit[1] == 3) {
                month = "MAR";
            } else if (dateSplit[1] == 4) {
                month = "APR";
            } else if (dateSplit[1] == 5) {
                month = "MAY";
            } else if (dateSplit[1] == 6) {
                month = "JUN";
            } else if (dateSplit[1] == 7) {
                month = "JUL";
            } else if (dateSplit[1] == 8) {
                month = "AUG";
            } else if (dateSplit[1] == 9) {
                month = "SEPT";
            } else if (dateSplit[1] == 10) {
                month = "OCT";
            } else if (dateSplit[1] == 11) {
                month = "NOV";
            } else if (dateSplit[1] == 12) {
                month = "DEC";
            }

            image.style.width = "auto";

            var row1 = document.createElement("tr");
            var row2 = document.createElement("tr");

            var imgTD = document.createElement("td");

            var eventNameTD = document.createElement("td");
            var eventName = document.createTextNode(nameLabel + playerCV[i].eventName);

            var dateTD = document.createElement("td");

            if (format == 1) {
                var date = document.createTextNode(dateLabel + dateSplit[0]);
            } else if (format == 2) {
                var date = document.createTextNode(dateLabel + month + " " + dateSplit[0]);
            } else {
                var date = document.createTextNode(dateLabel + playerCV[i].eventDate);
            }

            if (variable == 1) {
                var placementTD = document.createElement("td");
                var placement = document.createTextNode(infoLabel + playerCV[i].information);
            } else {

            }

            var cityTD = document.createElement("td");
            var city = document.createTextNode(cityLabel + playerCV[i].eventCity);

            imgTD.setAttribute("class", "imageCell");
            imgTD.setAttribute("rowspan", "2");
            imgTD.appendChild(image);

            eventNameTD.appendChild(eventName);
            dateTD.appendChild(date);

            if (variable == 1) {
                placementTD.appendChild(placement);
                row2.appendChild(placementTD);
            }

            cityTD.appendChild(city);

            row1.appendChild(imgTD);
            row1.appendChild(eventNameTD);
            row1.appendChild(dateTD);

            row2.appendChild(cityTD);

            if (variable == 0) {
                cityTD.setAttribute("colspan", "2");
            } else {
            }

            eve.appendChild(row1);
            eve.appendChild(row2);
        }
    }
}
