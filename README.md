# Local Development Database Setup

Nothing special. Just set up Apache with PHP, and point it to the public_html folder.

Make sure you create a symbolic link to the playerbench-api repository directory and name it "slim" in the public_html folder.
